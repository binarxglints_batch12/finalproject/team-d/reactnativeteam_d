import axios from 'axios';

export const openTrip = axios.create({
  baseURL: 'https://fp-open-trip.herokuapp.com/api/ot/',
});

export const url = 'https://fp-open-trip.herokuapp.com/api/ot/';
