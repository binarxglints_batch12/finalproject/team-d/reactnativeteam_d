export const COLORS = {
  red: '#FF385C',
  black: '#4A4A4A',
  softBlack: '#CCCCCC',
  white: '#FFFFFF',
  yellow: '#F2C94C',
  yellowBorder: '#F2C94C33',
  grey: '#F5F5F5',
};
