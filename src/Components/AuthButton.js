import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Button} from 'react-native-elements';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
//import from utils
import {COLORS} from '../Utils/Color';

export default function AuthButton(props) {
  const styles = StyleSheet.create({
    textStyle: {
      color: 'white',
      fontFamily: 'Inter-Bold',
      fontSize: moderateScale(15),
    },
    textline: {
      color: COLORS.red,
      fontFamily: 'Inter-Bold',
      fontSize: moderateScale(15),
    },

    container: {
      backgroundColor: COLORS.red,
      borderRadius: widthPercentageToDP(2),
      height: moderateScale(60),
      // margin: moderateScale(8),
      alignItems: 'center',
      ...props.styleFull,
    },

    outline: {
      borderRadius: widthPercentageToDP(2),
      height: moderateScale(60),
      width: widthPercentageToDP(95),
      marginLeft: moderateScale(8),
      alignItems: 'center',
      borderWidth: moderateScale(1),
      borderColor: COLORS.red,
      color: COLORS.red,
      backgroundColor: 'white',
      ...props.styleOut,
    },

    miniStyle: {
      borderRadius: widthPercentageToDP(1),
      height: moderateScale(35),
      width: widthPercentageToDP(20),
      alignItems: 'center',
      borderWidth: moderateScale(1),
      borderColor: COLORS.red,
      backgroundColor: 'white',
    },
    miniText: {
      color: COLORS.red,
      fontFamily: 'Inter',
      fontSize: moderateScale(12),
    },
    fullContainer: {
      justifyContent: 'center',
      width: widthPercentageToDP(95),
      borderRadius: widthPercentageToDP(2),
      ...props.fullContainerAdd,
    },
    fullMiniContainer: {
      justifyContent: 'center',
      width: widthPercentageToDP(95),
      borderRadius: widthPercentageToDP(2),
      ...props.fullMiniContainerAdd,
    },
  });

  return (
    <View style={props.center ? {alignItems: 'center'} : {}}>
      <Button
        raised={props.full ? true : false}
        title={props.judul}
        titleStyle={
          props.full
            ? styles.textStyle
            : props.mini
            ? styles.miniText
            : styles.textline
        }
        containerStyle={
          props.full ? styles.fullContainer : {justifyContent: 'center'}
        }
        buttonStyle={
          props.full
            ? styles.container
            : props.mini
            ? styles.miniStyle
            : styles.outline
        }
        onPress={props.submit}
        disabled={props.disabled}
        test={styles.container}
      />
    </View>
  );
}
