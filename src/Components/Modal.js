import React, {useState} from 'react';

import Modal from 'react-native-modal';

import {View, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';

import {
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

const ModalSwipeable = props => {
  return (
    <Modal
      isVisible={props.isVisible}
      swipeDirection="down"
      // swipeThreshold={0}
      propagateSwipe={true}
      scrollOffset={props.scrollOffset}
      scrollOffsetMax={props.scrollOffsetMax}
      //   onSwipeComplete={props.onSwipeComplete}
      onBackButtonPress={props.onBackButtonPress}
      onBackdropPress={props.onBackdropPress}
      hasBackdrop={true}
      style={{justifyContent: 'flex-start', margin: 0}}>
      <View style={styles.modalBackground}>
        <View style={styles.modal}>
          <TouchableOpacity activeOpacity={0.7} onPress={props.onClose}>
            <View style={styles.close} />
          </TouchableOpacity>
          {props.children}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  modal: {
    alignItems: 'center',
    width: widthPercentageToDP(100),
    // height: 'auto',
    height: heightPercentageToDP(90),
    backgroundColor: '#ffffff',
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
    paddingHorizontal: moderateScale(16),
    paddingBottom: moderateScale(16),
  },
  container: {
    marginTop: moderateScale(25),
    height: moderateScale(70),
    justifyContent: 'space-between',
  },
  placeholderText: {
    fontSize: moderateScale(12),
    color: '#C2C3C6',
  },
  containerText: {
    height: moderateScale(44),
    width: moderateScale(337),
    borderWidth: moderateScale(1),
    borderColor: '#EAEAEA',
    borderRadius: 3,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: moderateScale(15),
  },
  close: {
    width: moderateScale(30),
    height: moderateScale(4),
    borderRadius: moderateScale(100),
    backgroundColor: '#D3D3D3',
    marginTop: moderateScale(12),
    marginBottom: moderateScale(20),
  },
});

export default ModalSwipeable;
