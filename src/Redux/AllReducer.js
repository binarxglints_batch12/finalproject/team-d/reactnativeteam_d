import {combineReducers} from 'redux';
import reducerDetailHome from '../Screen/Home/HomeDetail/Redux/ReducerDetailHome';
import reducerGlobal from './GlobalReducer';

//register
import UserRegisterReducer from '../Screen/Register/Redux/User/UserRegisterReducer';
import HosterRegisterReducer from '../Screen/Register/Redux/Hoster/HosterRegisterReducer';

//Login
import LoginReducer from '../Screen/Login/Redux/LoginReducer';

//Home
import reducerHomePage from '../Screen/Home/Redux/ReducerHome';
import reducerHomeCalender from '../Screen/Home/HomeCalender/Redux/reducerHomeCalender';
import reducerCalender from '../Screen/Calender/Redux/reducerCalender';

//SearchTrip
import reducerSearchTrip from '../Screen/Trip/Redux/ReducerSearchTrip';

//PROFILE
import ReducerProfile from '../Screen/Profile/Redux/ReducerProfile';
import ReducerOrder from '../Screen/Order/Redux/ReducerOrder';

const AllReducer = combineReducers({
  DetailHome: reducerDetailHome,
  Global: reducerGlobal,
  UserRegister: UserRegisterReducer,
  HosterRegister: HosterRegisterReducer,
  Login: LoginReducer,
  ReducerHomePage: reducerHomePage,
  ReducerHomeCalender: reducerHomeCalender,
  ReducerSearchTrip: reducerSearchTrip,
  Profile: ReducerProfile,
  ReducerCalender: reducerCalender,
  Order: ReducerOrder,
});

export default AllReducer;
