export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCES';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const SET_USER_DATA = 'SET_USER_DATA';

export const LOGOUT = 'LOGOUT';

export const loginAction = payload => {
  return {type: LOGIN, payload};
};

export const loginFailed = payload => {
  return {type: LOGIN_FAILED, payload};
};

export const setDataLogin = payload => {
  return {type: LOGIN_SUCCESS, payload};
};

export const logoutAction = () => {
  return {type: LOGOUT};
};

export const setUserData = payload => {
  return {type: SET_USER_DATA, payload};
};
