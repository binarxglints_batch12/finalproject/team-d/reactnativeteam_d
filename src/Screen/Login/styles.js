import {StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

const styles = StyleSheet.create({
  subtitlepagetext: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingBottom: moderateScale(10),
  },
  subtitlesignin: {
    fontSize: 12,
    textAlign: 'center',
    color: '#9098B1',
    marginBottom: 20,
  },
  forgotpass: {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#FF385C',
    paddingBottom: 20,
    marginTop: 30,
  },
  textreg: {
    flexDirection: 'row',
    justifyContent: 'center',
    // backgroundColor: 'yellow',
  },
  input: {
    margin: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
  },
  reg: {
    color: '#FF385C',
    fontWeight: 'bold',
    fontSize: 12,
    marginLeft: moderateScale(4),
  },
});

export default styles;
