import {
  SET_NEW_USER_FAILED,
  SET_NEW_USER_SUCCESS,
} from '../User/ActionUserRegister';

const initialState = {
  data: [],
  message: '',
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_NEW_USER_SUCCESS:
      return {
        ...state,
        data: payload,
        message: payload.statusText,
      };
    case SET_NEW_USER_FAILED:
      return {
        ...state,
        message: payload,
      };

    default:
      return state;
  }
};
