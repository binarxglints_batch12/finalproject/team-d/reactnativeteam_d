export const POST_NEW_HOSTER = 'POST_NEW_HOSTER';
export const SET_NEW_HOSTER = 'SET_NEW_HOSTER';

export const PostNewHoster = payload => ({type: POST_NEW_HOSTER, payload});
export const SetNewHoster = payload => ({type: SET_NEW_HOSTER, payload});
