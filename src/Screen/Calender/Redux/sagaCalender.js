import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {GET_CALENDER, setDataByDate} from './actionCalender';
import {
  actionFailed,
  actionLoading,
  actionSuccess,
} from '../../../Redux/GlobalAction';

import {getDataHomeFailed} from '../../Home/Redux/ActionHome';
import {navigate} from '../../../Function/nav';

function* sagaGetDataCalender(action) {
  try {
    const {data, dateName} = action.payload;
    yield put(actionLoading(true));
    const res = yield axios.get(
      `https://fp-open-trip.herokuapp.com/api/ot/trip/date/search?start=${data}&end=${data}&page=0&limit=3`,
    );
    console.log(res, 'ISI NYA SAGA CALENDER');

    if (res.status === 200) {
      yield put(actionSuccess(true));
      yield put(setDataByDate({data: res.data.result, dateName}));
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getDataHomeFailed(error.response.data.message));
    yield navigate('Main');
    yield put(actionLoading(false));
  }
}

function* sagaCalender() {
  yield takeLatest(GET_CALENDER, sagaGetDataCalender);
}

export default sagaCalender;
