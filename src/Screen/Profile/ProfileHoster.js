import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {Input, Avatar} from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign';
import Iconlist from 'react-native-vector-icons/FontAwesome5';

//Componen
import Logo from '../../Components/Logo';
import AuthButton from '../../Components/AuthButton';
import {COLORS} from '../../Utils/Color';

//page
import styles from './styles';

const ProfileHoster = props => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Logo />
        <View style={styles.contmenuhoster}>
          <TouchableOpacity>
            <View style={styles.menuhoster}>
              <Icon name="scan1" size={35} color={COLORS.red} />
              <Text style={{color: COLORS.red}}>Scan code </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.menuhoster}>
              <Iconlist name="clipboard-list" size={35} color={COLORS.red} />
              <Text style={{color: COLORS.red}}>booking list </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.menuhoster}>
              <Iconlist
                name="file-invoice-dollar"
                size={35}
                color={COLORS.red}
              />
              <Text style={{color: COLORS.red}}>Invoice list </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.footerhost}>
        <View style={styles.avatar}>
          <TouchableOpacity>
            <Avatar
              rounded
              size="large"
              Image
              source={require('../../Assets/Images/luffy.jpg')}>
              <Avatar.Accessory
                name="user-alt"
                type="font-awesome-5"
                color="white"
                size={30}
              />
            </Avatar>
          </TouchableOpacity>
        </View>
        <View>
          <Input
            inputContainerStyle={{
              borderWidth: 1,
              paddingLeft: moderateScale(15),
              borderRadius: 5,
              borderColor: 'white',
            }}
            placeholder="Full Name"
            style={{color: 'white', fontSize: 14}}
            leftIcon={<Icon name="user" size={20} color="white" />}
          />
          <Input
            inputContainerStyle={{
              borderWidth: 1,
              paddingLeft: moderateScale(15),
              borderRadius: 5,
              borderColor: 'white',
            }}
            placeholder="email"
            style={{fontSize: 14}}
            leftIcon={<Icon name="mail" size={20} color="white" />}
          />
          <Input
            inputContainerStyle={{
              borderWidth: 1,
              paddingLeft: moderateScale(15),
              borderRadius: 5,
              borderColor: 'white',
            }}
            placeholder="password"
            secureTextEntry
            style={{fontSize: 14}}
            leftIcon={<Icon name="lock1" size={20} color="white" />}
          />

          <View style={styles.bottomreg}>
            <AuthButton
              submit={() => props.navigation.navigate('Register')}
              style={{width: moderateScale(150), height: moderateScale(50)}}
              judul="Back"
            />
            <AuthButton
              submit={() => props.navigation.navigate('RegisterHoster2')}
              style={{width: moderateScale(150), height: moderateScale(50)}}
              judul="Log Out"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default ProfileHoster;
