import {
  SET_PROFILE_FAILED,
  SET_PHOTO,
  SET_ORDER_ID_FAILED,
  SET_ORDER_ID_SUCCESS,
} from './ActionProfile';

const initialState = {
  data: [],
  newData: null,
  message: '',
  foto: null,
  voucher: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_PROFILE_FAILED:
      return {
        ...state,
        message: payload.message,
      };
    case SET_PHOTO:
      return {
        ...state,
        foto: payload,
      };
    case SET_ORDER_ID_SUCCESS:
      return {
        ...state,
        voucher: payload,
      };
    case SET_ORDER_ID_FAILED:
      return {
        ...state,
        message: payload,
      };

    default:
      return state;
  }
};
