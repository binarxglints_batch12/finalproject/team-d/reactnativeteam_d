import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {
  getDataHomeFailed,
  GET_CATEGORY,
  GET_DATA_BY_MONTH,
  setDataByMonth,
  setDataHome,
} from './ActionHome';

import {actionFailed, actionLoading} from '../../../Redux/GlobalAction';
import {navigate} from '../../../Function/nav';
import {openTrip} from '../../../Utils/baseurl';
function* sagaGetDataHome() {
  try {
    yield put(actionLoading(true));
    const res = yield axios.get(
      'https://fp-open-trip.herokuapp.com/api/ot/category/all',
    );
    // console.log(res, 'ISI NYA APA');

    if (res.status === 200) {
      yield put(setDataHome(res.data.result));
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getDataHomeFailed(error.response.data.message));
    yield navigate('LoginScreen');
    yield put(actionLoading(false));
  }
}
function* sagaGetDataByMonth(action) {
  try {
    const {start, end, monthName} = action.payload;
    yield put(actionLoading(true));
    const res = yield openTrip({
      url: `trip/date/search?start=${start}&end=${end}&page=0&limit=9`,
      validateStatus: status => status < 505,
    });
    // console.log(res, 'ISI NYA APA');

    if (res.status === 200) {
      yield put(setDataByMonth({data: res.data.result, monthName}));
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getDataHomeFailed(error.response.data.message));
    yield navigate('LoginScreen');
    yield put(actionLoading(false));
  }
}

function* sagaHome() {
  yield takeLatest(GET_CATEGORY, sagaGetDataHome);
  yield takeLatest(GET_DATA_BY_MONTH, sagaGetDataByMonth);
}

export default sagaHome;
