// react
import {StyleSheet} from 'react-native';

// library
import {moderateScale} from 'react-native-size-matters';

const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  backgroundHome: {
    backgroundColor: '#f3f3f3',
    elevation: 10,
  },
  buttonContainer: {
    marginLeft: moderateScale(7),
    paddingTop: moderateScale(15),
  },
  button: {
    flexDirection: 'row',
    marginBottom: moderateScale(10),
  },
  scroll: {
    marginBottom: moderateScale(200),
  },
  homeDesc: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: moderateScale(7),
    marginRight: moderateScale(15),
    marginTop: moderateScale(30),
  },
  desc: {
    fontWeight: 'bold',
    fontSize: moderateScale(16),
    letterSpacing: 0.5,
  },
  descSeeMore: {
    fontWeight: 'bold',
    fontSize: moderateScale(14),
    color: '#F24B5B',
    letterSpacing: 0.5,
  },
  card: {
    marginTop: moderateScale(15),
  },
  cardImage: {
    width: moderateScale(150),
    height: moderateScale(250),
    borderRadius: moderateScale(30),
    marginHorizontal: moderateScale(6),
    // opacity: 1,
    elevation: 2,
  },
  textBymMonthContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: moderateScale(20),
  },
  cardContainer: {
    // flex: 1,
    alignItems: 'baseline',
    justifyContent: 'flex-end',
    width: moderateScale(130),
    height: moderateScale(250),
  },
  placeName: {
    position: 'absolute',
    marginTop: moderateScale(8),
    left: moderateScale(15),
    bottom: moderateScale(40),
    fontSize: moderateScale(14),
    letterSpacing: 0.5,
  },
  // rating: {
  //   position: 'absolute',
  //   marginTop: moderateScale(6),
  //   left: moderateScale(15),
  //   bottom: moderateScale(85),
  // },
  iconMap: {
    position: 'absolute',
    marginTop: moderateScale(10),
    left: moderateScale(15),
    bottom: moderateScale(20),
  },
  locName: {
    position: 'absolute',
    color: 'white',
    marginTop: moderateScale(10),
    fontSize: moderateScale(10),
    left: moderateScale(33),
    bottom: moderateScale(20),
  },
  durTrip: {
    position: 'absolute',
    color: 'white',
    marginTop: moderateScale(10),
    fontSize: moderateScale(10),
    right: moderateScale(15),
    bottom: moderateScale(20),
  },
});

export default styles;
