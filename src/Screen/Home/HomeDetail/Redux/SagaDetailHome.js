import {takeLatest, put} from 'redux-saga/effects';
import {openTrip} from '../../../../Utils/baseurl';
import {getDetailError, getDetailSuccess, GET_DETAIL} from './ActionHomeDetail';
import {
  actionFailed,
  actionSuccess,
  actionLoading,
} from '../../../../Redux/GlobalAction';
import {navigate} from '../../../../Function/nav';

function* sagaDetailHome(action) {
  try {
    yield put(actionLoading(true));
    const res = yield openTrip({
      url: `trip?id=${action.payload}&page=0&limit=4`,
      method: 'GET',
      validateStatus: status => status < 505,
    });
    console.log(res, 'Home Detail KUY');

    if (res.status === 200) {
      yield put(actionSuccess(true));
      yield put(getDetailSuccess(res.data.result));
      yield navigate('HomeDetail');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(getDetailError(error.response.data.message));
    yield navigate('Home');
    yield put(actionLoading(false));
  }
}

function* SagaDetail() {
  yield takeLatest(GET_DETAIL, sagaDetailHome);
}

export default SagaDetail;
