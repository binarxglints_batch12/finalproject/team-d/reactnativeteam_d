export const GET_HOME_CALENDER = 'GET_HOME_CALENDER';
export const SET_HOME_CALENDER = 'SET_HOME_CALENDER';

export const setHomeCalender = payload => {
  return {
    type: 'SET_HOME_CALENDER',
    payload,
  };
};

export const getHomeCalender = payload => {
  return {
    type: 'GET_HOME_CALENDER',
    payload,
  };
};
