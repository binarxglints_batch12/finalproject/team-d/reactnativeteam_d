import {
  SET_DATA_BANK_SUCCESS,
  SET_DATA_BANK_FAILED,
  SET_ORDER_SUCCESS,
  SET_ORDER_FAILED,
} from './ActionOrder';

const initialState = {
  bank: [],
  message: '',
  order: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_DATA_BANK_SUCCESS:
      return {...state, bank: payload};
    case SET_DATA_BANK_FAILED:
      return {...state, message: payload};
    case SET_ORDER_SUCCESS:
      return {...state, order: payload};
    case SET_ORDER_FAILED:
      return {...state, message: payload};

    default:
      return state;
  }
};
