import React from 'react';

import Mandiri from './banklogo/mandiri.svg';
import Bni from './banklogo/bni.svg';
import Bri from './banklogo/bri.svg';
import Btn from './banklogo/btn.svg';
import Danamon from './banklogo/danamon.svg';
import Bukopin from './banklogo/bukopin.svg';
import Mega from './banklogo/mega.svg';
import Muamalat from './banklogo/muamalat.svg';
import Cimb from './banklogo/cimb.svg';
import Ekonomi from './banklogo/ekonomi.svg';
import Sinarmas from './banklogo/sinarmas.svg';
import Mnc from './banklogo/mnc.svg';
import Bca from './banklogo/bca.svg';

const banks = {
  Mandiri,
  Bni,
  Bri,
  Btn,
  Danamon,
  Bukopin,
  Mega,
  Muamalat,
  Cimb,
  Ekonomi,
  Sinarmas,
  Mnc,
  Bca,
};

export default function Banks({file, width, height}) {
  const HostBank = banks[file];
  return <HostBank width={width} height={height} />;
}

// export const listImageBank = [
//   {
//     bank_name: 'mandiri',
//     image: require('./banklogo/mandiri.svg'),
//   },
//   {
//     bank_name: 'bni',
//     image: bni,
//   },
//   {
//     bank_name: 'bri',
//     image: bri,
//   },
//   {
//     bank_name: 'btn',
//     image: btn,
//   },
//   {
//     bank_name: 'danamon',
//     image: danamon,
//   },
//   {
//     bank_name: 'bukopin',
//     image: bukopin,
//   },
//   {
//     bank_name: 'mega',
//     image: mega,
//   },
//   {
//     bank_name: 'muamalat',
//     image: muamalat,
//   },
//   {
//     bank_name: 'cimb',
//     image: cimb,
//   },
//   {
//     bank_name: 'ekonomi',
//     image: ekonomi,
//   },
//   {
//     bank_name: 'sinarmas',
//     image: sinarmas,
//   },
//   {
//     bank_name: 'mnc',
//     image: mnc,
//   },
//   {
//     bank_name: 'bca',
//     image: bca,
//   },
// ];
